### What was refactored
* Proper names given (such as CarsAPI -> ProductsAPI)
* Scenarios points and implementation of tests were split (SearchStepsDefinitions, ProductsAPI)
* Made test methods reusable
* Added helpers
* Added a file reader and synonyms.txt to support English/Dutch/synonyms (orange, sinas; pasta, farfalle, etc)
* Cleaned up the Gradle build file
  * compile to implementation for "ch.qos.logback:logback-classic:${logbackVersion}"
  * removed plugin: 'eclipse'
* Removed Maven files and folders due to non-use
* Removed a Github Actions flow folder due to non-use
* Removed the history folder due to non-use
* Added a CI file for Gitlab

### How to install it, run it, and write new tests
1. ```./gradlew clean build```
2. ```./gradlew test``` or run it via ```TestRunner```
3. To add new test you
   * Write a new scenario to **post_product.feature**
   * Write support code for this scenario to **SearchStepsDefinitions**
   * Write a test implementation to **ProductsAPI**
   * Update **synonyms.txt** (if necessary)
4. An HTML reporting file (index.html) is generated in /build/reposts/tests/test
