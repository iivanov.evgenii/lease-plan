package starter.reader;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class SynonymsFileReader extends FileReader<Map<String, List<String>>> {

    private static final String FILE_PATH = "src/test/resources/synonyms.txt";

    public SynonymsFileReader() {
        this.filePath = FILE_PATH;
        map = new HashMap<>();
        readFile();
    }

    @Override
    public Map<String, List<String>> getData() {
        return map;
    }

    @Override
    protected void readFile() {
        try {
            String currentKey = null;
            for (String line : Files.readAllLines(Paths.get(filePath))) {
                if (!line.isEmpty()) {
                    if (currentKey == null) {
                        currentKey = line.trim().toLowerCase();
                    } else {
                        List<String> values = Arrays.asList(line.trim().toLowerCase().split(",\\s*"));
                        map.put(currentKey, values);
                        currentKey = null;
                    }
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
