package starter.reader;

import java.util.List;
import java.util.Map;

public abstract class FileReader<T> {

    protected String filePath;
    protected Map<String, List<String>> map;

    public abstract T getData();
    protected abstract void readFile();
}
