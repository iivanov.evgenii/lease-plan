package starter.helper;

import java.util.List;
import java.util.Map;
import java.util.Objects;

import starter.exception.NoSynonymsFoundException;
import starter.reader.FileReader;
import starter.reader.SynonymsFileReader;

public class SynonymsHelper {

    private final Map<String, List<String>> synonyms;
    private final FileReader<Map<String, List<String>>> reader;

    public static final SynonymsHelper INSTANCE = new SynonymsHelper();

    private SynonymsHelper() {
        this.reader = new SynonymsFileReader();
        this.synonyms = reader.getData();
    }

    @SuppressWarnings("uncheked")
    public List<String> getSynonymsForWord(String word) throws NoSynonymsFoundException {
        List<String> foundSynonyms = synonyms.get(word);

        if (Objects.isNull(foundSynonyms)) {
            throw new NoSynonymsFoundException(String.format("Found no synonyms for '%s'. Please fill the 'synonyms.txt'", word));
        }

        return foundSynonyms;
    }

}
