package starter.exception;

public class NoSynonymsFoundException extends Exception {
    public NoSynonymsFoundException(String message) {
        super(message);
    }
}
