package starter.helper;

import org.hamcrest.Matcher;
import org.hamcrest.Matchers;

import java.util.List;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class TestHelper {

    private static final String URL_PATTERN = "^(https?|ftp)://[\\w-]+(\\.[\\w-]+)+([/?#][^\\s]*)?$";

    public static final Pattern URL_COMPILED_PATTERN = Pattern.compile(URL_PATTERN);

    public static Matcher<Iterable<? super String>> buildSynonymsMatcher(List<String> strings) {
        List<Matcher<? super String>> matchers = strings.stream()
            .map(Matchers::containsStringIgnoringCase)
            .collect(Collectors.toList());

        return Matchers.hasItem(Matchers.anyOf(matchers));
    }
}
