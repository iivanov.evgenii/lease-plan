package starter.helper;

import java.util.regex.Pattern;

public class JsonNames {
    public static final String ERROR = "detail.error";
    public static final String TITLE = "title";
    public static final String PRICE = "price";
    public static final String URL = "url";
}
