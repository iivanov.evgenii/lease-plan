package starter.stepdefinitions;

import net.serenitybdd.rest.SerenityRest;
import net.thucydides.core.annotations.Step;

import org.junit.Assert;

import java.util.List;
import java.util.Map;

import io.restassured.response.Response;
import starter.exception.NoSynonymsFoundException;
import starter.helper.JsonNames;
import starter.helper.SynonymsHelper;
import starter.helper.TestHelper;

import static net.serenitybdd.rest.SerenityRest.restAssuredThat;
import static org.hamcrest.CoreMatchers.allOf;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.matchesPattern;
import static org.hamcrest.core.Every.everyItem;
import static org.hamcrest.core.IsNot.not;
import static org.hamcrest.text.IsEmptyString.emptyString;

public class ProductsAPI {

    @Step("Call API endpoint: {0}")
    public void callEndpoint(String endpoint) {
        SerenityRest.given().get(endpoint);
    }

    @Step("Verify that API responses less then {0}")
    public void verifyResponseTime(long expectedMilliSeconds) {
        Response response = SerenityRest.then().extract().response();
        long actualMilliseconds = response.getTime();
        Assert.assertTrue(actualMilliseconds < expectedMilliSeconds);
    }

    @Step("Verify empty cookies")
    public void verifyEmptyCookies() {
        Response response = SerenityRest.then().extract().response();
        Map<String, String> cookies = response.getCookies();
        Assert.assertTrue(cookies.isEmpty());
    }

    @Step("Verify {0} status code")
    public void verifyStatusCode(int expectedCode) {
        Response response = SerenityRest.then().extract().response();
        int actualStatusCode = response.getStatusCode();
        Assert.assertEquals(expectedCode, actualStatusCode);
    }

    @Step("Verify content type is {0}")
    public void verifyContentType(String expectedContentType) {
        Response response = SerenityRest.then().extract().response();
        String actualContentType = response.contentType();
        Assert.assertEquals(expectedContentType, actualContentType);
    }

    @Step("Verify no results")
    public void verifyNoResults() {
        restAssuredThat(response -> response.body(JsonNames.ERROR, equalTo(true)));
    }

    @Step("Verify results displayed for: {0}")
    public void verifyResultsDisplayed(String expectedText) {
        List<String> synonyms;
        try {
            synonyms = SynonymsHelper.INSTANCE.getSynonymsForWord(expectedText);
        } catch (NoSynonymsFoundException e) {
            throw new RuntimeException(e);
        }
        restAssuredThat(response -> response.body(JsonNames.TITLE, TestHelper.buildSynonymsMatcher(synonyms)));
    }

    @Step("Verify results with prices")
    public void verifyResultsWithPrices() {
        restAssuredThat(response -> response.body(JsonNames.PRICE, everyItem(not(emptyString()))));
    }

    @Step("Verify results with product urls")
    public void verifyResultsWithProductUrls() {
        restAssuredThat(response -> response.body(
            JsonNames.URL,
            everyItem(allOf(not(emptyString()),
            matchesPattern(TestHelper.URL_COMPILED_PATTERN))))
        );
    }
}
