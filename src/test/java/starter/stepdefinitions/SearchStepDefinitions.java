package starter.stepdefinitions;

import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import net.thucydides.core.annotations.Steps;

public class SearchStepDefinitions {

    @Steps
    public ProductsAPI productsAPI;

    @When("he calls endpoint {string}")
    public void heCallsEndpoint(String endpoint) {
        productsAPI.callEndpoint(endpoint);
    }

    @Then("he sees the response time less than {long}")
    public void heSeesTheResponseTimeLessThan(long milliseconds) {
        productsAPI.verifyResponseTime(milliseconds);
    }

    @Then("he sees the response with empty cookies")
    public void heSeesTheResponseTimeLessThan() {
        productsAPI.verifyEmptyCookies();
    }

    @Then("he sees the content type for {string}")
    public void heSeesJsonContentType(String contentType) {
        productsAPI.verifyContentType(contentType);
    }

    @Then("he sees the {int} code for existed product")
    public void heSeesThe200Code(int code) {
        productsAPI.verifyStatusCode(code);
    }

    @Then("he sees the {int} code for a product that does not support")
    public void heSeesThe404Code(int code) {
        productsAPI.verifyStatusCode(code);
    }

    @Then("he does not see the results")
    public void heDoesNotSeeTheResults() {
        productsAPI.verifyNoResults();
    }

    @Then("he sees results displayed for {string}")
    public void heSeesTheResultsDisplayedFor(String expectedText) {
        productsAPI.verifyResultsDisplayed(expectedText);
    }

    @Then("he sees results with prices")
    public void heSeesTheResultsWithPrices() {
        productsAPI.verifyResultsWithPrices();
    }

    @Then("he sees results with product urls")
    public void heSeesTheResultsWithProductUrls() {
        productsAPI.verifyResultsWithProductUrls();
    }
}
