Feature: Test a product data endpoint

### Please use endpoint GET https://waarkoop-server.herokuapp.com/api/v1/search/demo/{product} for getting the products.
### Available products: "orange", "apple", "pasta", "cola"
### Prepare Positive and negative scenarios

  Scenario: Verify no results for a product that does not support
    When he calls endpoint "https://waarkoop-server.herokuapp.com/api/v1/search/demo/bread"
    Then he does not see the results

  Scenario: Verify API response time less than 1.5 seconds
    When he calls endpoint "https://waarkoop-server.herokuapp.com/api/v1/search/demo/cola"
    Then he sees the response time less than 1500

  Scenario: Verify that cookies are empty
    When he calls endpoint "https://waarkoop-server.herokuapp.com/api/v1/search/demo/cola"
    Then he sees the response with empty cookies

  Scenario: Verify success status code for an existed product
    When he calls endpoint "https://waarkoop-server.herokuapp.com/api/v1/search/demo/cola"
    Then he sees the 200 code for existed product

  Scenario: Verify content type is Json
    When he calls endpoint "https://waarkoop-server.herokuapp.com/api/v1/search/demo/cola"
    Then he sees the content type for "application/json"

  Scenario: Verify not found status code for a product that does not support
    When he calls endpoint "https://waarkoop-server.herokuapp.com/api/v1/search/demo/car"
    Then he sees the 404 code for a product that does not support

  Scenario: Verify results for orange
    When he calls endpoint "https://waarkoop-server.herokuapp.com/api/v1/search/demo/orange"
    Then he sees results displayed for "orange"
    Then he sees results with prices
    Then he sees results with product urls

  Scenario: Verify results for pasta
    When he calls endpoint "https://waarkoop-server.herokuapp.com/api/v1/search/demo/pasta"
    Then he sees results displayed for "pasta"
    Then he sees results with prices
    Then he sees results with product urls